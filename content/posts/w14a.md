---
title: "Week 15. MDS II: How + Implementation and Applications"
date: 2023-05-15T11:02:20+08:00
draft: false
tags: [MDS, distance matrix, flavor network, music map, scent map, wordle]
categories: [notes]
markup: mmark
---
<img src="https://i.pinimg.com/736x/5f/c7/7c/5fc77c2b2be7e0ecaf457ddf26336312--my-website-art-paintings.jpg" style="zoom:67%;" />

## Application*

* [Wordle](https://uploads-ssl.webflow.com/5d9184d5124e07e9fe1b802a/5e7815b69f6c13c2f5b41881_Chatdesk_coronavirus_infographic_word_cloud_COVID-19.png), [Flavor network and the principles of food pairing](https://www.nature.com/articles/srep00196), [Flavor Network](https://foodgalaxy.jp/FlavorNetwork/)
* [Fragrance Wheel](https://www.candlescience.com/the-fragrance-wheel-notes-families-and-how-to-use-it/)
* [Liu (2020). 食圖:風味食材的視覺化](http://etd.ndhu.edu.tw/cgi-bin/gs32/gsweb.cgi/ccd=RM5Zb6/record?r1=2&h1=0), [Hou (2016) MDS文字雲：以Ptt八卦版為例 ](http://etd.ndhu.edu.tw/cgi-bin/gs32/gsweb.cgi/ccd=RM5Zb6/record?r1=5&h1=0). [Liu (2021). 香水圖：以 t-SNE 建構之資料視覺化](https://etd.ndhu.edu.tw/cgi-bin/gs32/gsweb.cgi/ccd=pz.utA/record?r1=1&h1=0).
* [Music Map](https://laughingsquid.com/interactive-music-map/), [Cars MindMap](https://coggle.it/diagram/WfyKNrjLMQABbKLJ/t/cars-expert-mind-map), 

## How

##### Theorem 17.1. ("Recovery of Coordinates")
Define $A = (a_{i j}), a_{i j} = -\frac{1}{2}d_{i j}^2$ and $B=HAH$ where $H$ is the centering matrix. 
* D is Euclidean if and only if $B$ is  postitive semi-definite.
* If $D$ is the distance matrix of a a data matrix then $B =HXX^{t}H$, $B$ is called the inner product matrix. 

### Implication
* $D \leadsto$ (centered) $XX^T$ (nxn) and $X^T X$ which allows data-vis or dimension reduction for these $n$ points in $R^q$ space and $q \leq r=rank(B) = rank(X^T X) = rank(X)$. 
* Note: there is **no** coordinates $x_i \in \mathcal{R}^p$ **nor** the $p$ known to us only $D$ is given.
* Choice of Virtual Dimension: Variation explained by $q$-dimensional DV or DR can be assessed by $$\tau_q = \frac{\sum_{j=1}^q \lambda_j}{\sum_{j=1}^r \lambda_j}.$$ 

##### Fix for $B$ is not positive-semidefinite
$d_{i j} \leftarrow 0$ if $i=j$; $d_{i j}  \leftarrow d_{i j} + e \geq 0$ if $i \neq j$ such that $B$ becomes postitive-semidefinite with a small rank. 

#### Proximity/Similarity vs. Distance/Dissimilarity between $x_i, x_j$

* $c_{i, j} = \max{d_{i j}} - d_{i j}$
* $d_{i j} = (c_{i i} - 2 c_{i j} + c_{j j})^{1/2}$  (Note: $d_{i j}^2 = b_{i j} = x_i^t x_j$)

